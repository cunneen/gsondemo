
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author cunnem
 */
public class Contact {
    private String firstName;
    private String lastName;
    private String nickName;
    private String title;
    private List<String> emailAddresses;
    
    private char mailFormat;
    private int displayFormat;

    public Contact(String firstName, String lastName, String nickName, String title) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName = nickName;
        this.title = title;
    }

    /**
     * add an email address to this contact
     * @param emailAddress 
     */
    public void addEmailAddress(String emailAddress) {
        if (this.emailAddresses == null) {
            this.emailAddresses = new ArrayList<String>();
        }
        
        this.emailAddresses.add(emailAddress);
    }
    
    
    // getters and setters
    public Contact() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getEmailAddresses() {
        return emailAddresses;
    }

    public void setEmailAddresses(List<String> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    public char getMailFormat() {
        return mailFormat;
    }

    public void setMailFormat(char mailFormat) {
        this.mailFormat = mailFormat;
    }

    public int getDisplayFormat() {
        return displayFormat;
    }

    public void setDisplayFormat(int displayFormat) {
        this.displayFormat = displayFormat;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.firstName);
        hash = 59 * hash + Objects.hashCode(this.lastName);
        hash = 59 * hash + Objects.hashCode(this.nickName);
        hash = 59 * hash + Objects.hashCode(this.title);
        hash = 59 * hash + Objects.hashCode(this.emailAddresses);
        hash = 59 * hash + this.mailFormat;
        hash = 59 * hash + this.displayFormat;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contact other = (Contact) obj;
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.nickName, other.nickName)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.emailAddresses, other.emailAddresses)) {
            return false;
        }
        if (this.mailFormat != other.mailFormat) {
            return false;
        }
        if (this.displayFormat != other.displayFormat) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Contact{" + "firstName=" + firstName + ", lastName=" + lastName + ", nickName=" + nickName + ", title=" + title + ", emailAddresses=" + emailAddresses + ", mailFormat=" + mailFormat + ", displayFormat=" + displayFormat + '}';
    }
    
    
    
}
