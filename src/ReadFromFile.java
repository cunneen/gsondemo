

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.*;
import java.nio.file.*;

public class ReadFromFile {
    
    public static Contact[] getContactsFromFile() {
        Contact[] sample2 = null;  // the item we're trying to retrieve
        Path path =
                Paths.get("SampleFile.txt");

        Gson gson = new GsonBuilder().create();
        try {
            InputStream input = new BufferedInputStream(Files.newInputStream(path));
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
             sample2 = gson.fromJson(reader, Contact[].class);


             reader.close();
        } catch (Exception e) {
            System.out.println("Message: " + e);
        }
        return sample2;
        
    }
    
    // this is just to test our ReadFromFile.getContactsFromFile() method.
    public static void main(String[] args) {
       // get all the contacts from the file.
       Contact[] allTheContacts = ReadFromFile.getContactsFromFile();
       // print out all the contacts we retrieved from the file.
        for (Contact contact : allTheContacts) {
            System.out.println("contact = " + contact);
        }
    }
}
