

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.*;
import java.nio.file.*;
import static java.nio.file.StandardOpenOption.*;
/**
 * Example utility class for writing objects as JSON to a file.
 * @author mikecunneen
 */
public class WriteToFile {
    /**
     * Example method that writes an array of Contact items to a file (as JSON).
     * @param objectsToAdd 
     */
    public static void addContactsToFile(Contact[] objectsToAdd) {
        // get the file path
        Path path =
                Paths.get("SampleFile.txt");

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        // Create a JSON String (from a Contact instance) to our file.
        String sample_JSon = gson.toJson(objectsToAdd);

        try {
            // set up our file writer
            OutputStream output = new BufferedOutputStream(Files.newOutputStream(path, CREATE, TRUNCATE_EXISTING));
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
            // write the JSON string to our file
            writer.write(sample_JSon);
            writer.flush();
            
            writer.close();
        } catch (Exception e) {
            System.out.println("Error message: " + e);
        }

    }

    // This is just to test our WriteToFile.addContactsToFile(...) mehod.
    public static void main(String[] args) {
        // Test the writing of JSON by creating an array of Contacts 
        // and using our method to write them to a file.
        
        Contact[] arrayOfThingsToPutInFile = new Contact[10];

        for (int j = 0; j < arrayOfThingsToPutInFile.length; j++) {
            Contact sample = new Contact("Mike" + j, "Cunneen" + j , "Mikey" + j, "Mr");
            
            // add child objects to our sample object
            for (int i = 0; i < 3; i++) {
                String emailAddress = "email" + i + j + "@example.com";
                sample.addEmailAddress(emailAddress);
            }
            
            arrayOfThingsToPutInFile[j] = sample;
        }
        // this does the work.
        WriteToFile.addContactsToFile(arrayOfThingsToPutInFile);
    }
}
