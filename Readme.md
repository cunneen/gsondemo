#GSONDemo

This is a simple example of how to use GSON to store and retrieve objects to/from a file.

It is packaged as a NetBeans project.

1. Download the [zip file here](https://bitbucket.org/cunneen/gsondemo/get/master.zip) (packaged as a NetBeans project).
2. Extract and import the files into your project.
3. Substitute the "Contact" class with your own class definition.
4. Replace references to "Contact" with references to your own class.

###For example (writing to a file): 

    :::java
        // Create an array of Contacts and write them to a file.
        Contact[] arrayOfThingsToPutInFile = new Contact[10];

        for (int j = 0; j < arrayOfThingsToPutInFile.length; j++) {
            Contact sample = new Contact("Mike" + j, "Cunneen" + j , "Mikey" + j, "Mr");
            
            // add child objects to our sample object
            for (int i = 0; i < 3; i++) {
                String emailAddress = "email" + i + j + "@example.com";
                sample.addEmailAddress(emailAddress);
            }
            
            arrayOfThingsToPutInFile[j] = sample;
        }
        // this does the work.
        WriteToFile.addContactsToFile(arrayOfThingsToPutInFile);

###For example (reading from the file): 
    :::java
       // get all the contacts from the file.
       Contact[] allTheContacts = ReadFromFile.getContactsFromFile();
       // print out all the contacts we retrieved from the file.
       for (Contact contact : allTheContacts) {
            System.out.println("contact = " + contact);
       }

